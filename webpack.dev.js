'use strict';

let webpackCommon = require('./webpack.common');
let webpackMerge = require('webpack-merge');

var webpackDev = {
  watch: true,
  watchOptions: {
    aggregateTimeout: 1000,
    poll: 1000,
    ignored: /node_modules/
  },
  devServer: {
    headers: { 'Access-Control-Allow-Origin': '*' }
  },
  devtool: 'cheap-inline-module-source-map',
};

webpackDev = webpackMerge(webpackCommon, webpackDev);

module.exports = webpackDev;
