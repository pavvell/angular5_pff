'use strict';

let webpack = require('webpack');
let webpackCommon = require('./webpack.common');
let webpackMerge = require('webpack-merge');

let webpackProd = {};

webpackProd = webpackMerge(webpackCommon, webpackProd);

webpackProd.plugins.push(
  new webpack.optimize.UglifyJsPlugin({
    beautify: false,
    output: {
      comments: false
    },
    mangle: false,
    compress: {
      screw_ie8: true,
      drop_console: false,
      drop_debugger: true,
      warnings: false,
      conditionals: false, // should be false, otherwise it breaks ie-shim
      unused: true,
      comparisons: true,
      sequences: true,
      dead_code: true,
      evaluate: true,
      if_return: true,
      join_vars: true,
      negate_iife: false // we need this for lazy v8
    },
    sourceMap: false
  })
);

module.exports = webpackProd;
