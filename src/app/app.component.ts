import { Component } from '@angular/core';
import { PnMessagesService } from 'piano-ff/src/components/messages/services/messages.service';

import 'piano-ff/src/components/messages/messages.scss';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  providers: [PnMessagesService]
})
export class AppComponent {

  constructor(private pnMessagesService: PnMessagesService) {}

  showMessage() {
    const message = this.pnMessagesService.showMessage({
      title: 'test title',
      text: 'some text <b>html</b>',
      __hasUndoButton: true,
      __hasCheckIcon: true,
    });

    message.result.subscribe(console.log, console.error);
  }
}
