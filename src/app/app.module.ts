import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

/* Pff modules */
import { MessagesModule } from 'piano-ff/src/components/messages/messages.module';

/* Pff styles */
import 'piano-ff/src/common/style/base.scss';
import 'piano-ff/src/elements/type/type.scss';
import 'piano-ff/src/elements/button/button.scss';

/* Module specific styles */
import './app.component.scss';

/* Module components */
import { AppComponent } from './app.component';

@NgModule({
  imports: [
    BrowserModule,
    MessagesModule
  ],
  providers: [],
  declarations: [
    AppComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
