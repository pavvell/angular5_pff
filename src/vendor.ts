import 'ie-shim';

import 'intl/index';
import 'intl/locale-data/jsonp/en.js';

import 'core-js/client/shim.js';
import 'reflect-metadata';
import 'zone.js';

import 'ts-helpers';

import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/router';
import '@angular/core';
import '@angular/forms';
import '@angular/common';
import '@angular/http';

import 'rxjs';

import 'lodash/cloneDeep';
import 'lodash/isEqual';
import 'lodash/uniq';
import 'lodash/isArray';
import 'lodash/isUndefined';
import 'lodash/isNull';

import 'moment';
import 'moment/locale/sv';
import 'moment/locale/ja';
import 'moment/locale/es';
import 'moment/locale/fr-ca';
import 'moment/locale/nl';
import 'moment/locale/ru';
import 'moment/locale/pt-br';
import 'moment/locale/de';
import 'moment/locale/nb';
import 'moment/locale/da';
import 'moment/locale/pl';
import 'moment/locale/sl';
import 'moment/locale/it';
import 'moment/locale/pt';
