'use strict';

let webpack = require('webpack');
let extractTextPlugin = require('extract-text-webpack-plugin');
let path = require('path');
let fs = require('fs');
let node_modules_dir = path.resolve(__dirname, './node_modules');
let dist_dir = path.resolve(__dirname, './dist');
let app_path = path.resolve(__dirname, './src');
let CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin;
let entries = {};
let sourceMap = true;
let isProdMode = process.env.NODE_ENV === 'production';

if (isProdMode) {
  sourceMap = false;
}

entries['app'] = path.resolve(__dirname, './src/main.ts');
entries['vendor'] = path.resolve(__dirname, './src/vendor.ts');

module.exports = {
  context: app_path,
  entry: entries,
  output: {
    path: dist_dir,
    filename: 'js/[name].bundle.js',
    chunkFilename: 'js/[id].bundle.js'
  },
  externals: {},
  module: {
    rules: [
      {
        test: /\.ts|\.tsx$/,
        include: [
          /\/node_modules\/piano-ff\//
        ],
        exclude: [
          /dist/,
          /\.(spec|e2e)\.ts$/
        ],
        use: [
          {
            loader: 'awesome-typescript-loader',
            options: {
              useCache: true,
              transpileOnly: true,
              sourceMap: sourceMap
            }
          },
          {
            loader: 'angular2-template-loader', options: { sourceMap: sourceMap }
          }
        ]
      },
      {
        test: /\.ts|\.tsx$/,
        exclude: [
          /dist/,
          /\.(spec|e2e)\.ts$/,
          /node_modules\/(?!(ng2-.+|ngx-.+))/
        ],
        use: [
          {
            loader: 'awesome-typescript-loader',
            options: {
              transpileOnly: true,
              useCache: true,
              sourceMap: sourceMap
            }
          },
          {
            loader: 'angular2-template-loader', options: { sourceMap: sourceMap }
          }
        ]
      },
      {
        test: /\.s?css$/,
        use: extractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
              loader: 'css-loader', options: { sourceMap: sourceMap }
            },
            {
              loader: 'postcss-loader', options: { sourceMap: sourceMap }
            },
            {
              loader: 'resolve-url-loader', options: { sourceMap: sourceMap }
            },
            {
              loader: 'sass-loader', options: { sourceMap: true }
            }
          ],
          publicPath: '../'
        })
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader'
          }
        ]
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'images/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.otf$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'fonts/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'fonts/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'fonts/[name].[ext]'
            }
          }
        ]
      }
    ]
  },
  node: {
    fs: false,
    Buffer: false
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    }),
    new extractTextPlugin('css/[name].bundle.css'),
    new CommonsChunkPlugin({
      name: ['vendor'],
      minChunks: Infinity
    }),
    new webpack.LoaderOptionsPlugin({
      options: {
        context: app_path,
        htmlLoader: {
          minimize: true,
          removeAttributeQuotes: false,
          caseSensitive: true,
          customAttrSurround: [
            [/#/, /(?:)/],
            [/\*/, /(?:)/],
            [/\[?\(?/, /(?:)/]
          ],
          customAttrAssign: [/\)?\]?=/]
        }
      }
    }),
    new webpack.ContextReplacementPlugin(
      /angular(\\|\/)core(\\|\/)/,
      path.resolve(app_path)
    ),
    new webpack.ContextReplacementPlugin(
      /moment[\/\\]locale$/,
      /sv|ja|es|fr-ca|nl|en|ru|pt-br|de|nb|da|pl|sl|it|pt/
    )
  ],
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.scss', '.css', '.json'],
    modules: [
      path.resolve(node_modules_dir, './piano-ff/src/components'),
      path.resolve(app_path),
      'node_modules'
    ],
    alias: {},
    enforceExtension: false
  },
  resolveLoader: {
    modules: ["node_modules"]
  },
  watch: false,
  watchOptions: {
    aggregateTimeout: 100
  },
  devtool: false,
  stats: {children: false}
};
