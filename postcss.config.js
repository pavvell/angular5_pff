path = require('path');

module.exports = (ctx) => ({
  plugins: {
    'postcss-inline-svg': {
      path: path.resolve('node_modules/piano-ff/src/svg')
    },
    'postcss-svgo': {
      removeAttrs: {
        attrs: [
          'fill',
          'stroke'
        ]
      },
      removeUselessDefs: true
    },
    'autoprefixer': {
      browsers: ['ie >= 9', '> 0.5%']
    }
  }
});
